<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

//Route::any('{all}',[App\Http\Controllers\HomeController::class, 'index'])
//    ->where('all', '^(?!api).*$')
//    ->where('all', '^(?!storage).*$');

Route::any('{all}',function (){
    return view('layouts.master');
})->where('all', '^(?!api).*$')
    ->where('all', '^(?!storage).*$');
