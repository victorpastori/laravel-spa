const routes = [
    {
        path: '/home',
        component: () => import('../Components/Container.vue'),
        children: [
            {
                path: '/',
                component: () => import('../Pages/Home.vue'),
                name: 'home',
            },
        ]
    },
    {
        path: '*',
        component: () => import('../Pages/NotFound.vue')
    }
]

export default routes;
